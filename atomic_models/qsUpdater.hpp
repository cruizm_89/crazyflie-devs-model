#ifndef BOOST_SIMULATION_PDEVS_QS_UPDATER_H
#define BOOST_SIMULATION_PDEVS_QS_UPDATER_H
#include <string>
#include <utility>
#include <map>
#include <memory>
#include <boost/simulation/pdevs/atomic.hpp>

#include "../data_structures/Message.hpp"


using namespace boost::simulation::pdevs;
using namespace boost::simulation;
using namespace std;

template<class TIME, class MSG>
class qsUpdater : public pdevs::atomic<TIME, MSG>{
private:


public:

  explicit qsUpdater() noexcept : {}

  void internal() noexcept {}

  TIME advance() const noexcept {}

  vector<MSG> out() const noexcept {}

  void external(const std::vector<MSG>& mb, const TIME& t) noexcept {}

  virtual void confluence(const std::vector<MSG>& mb, const TIME& t) noexcept {}

  /***************************************
  ********* helper functions *************
  ***************************************/
};

#endif // BOOST_SIMULATION_PDEVS_QS_UPDATER_H
